<?php

namespace app\utils;

use app\model\User;
use Slim\Slim;

class Authentication {

    const VISITOR = 0;
    const USER = 1;
    const MODERATOR = 2;
    const ADMIN = 3;
    const SUPER_ADMIN = 4;

    public static function authenticate($email, $password) {
        $result = User::where('email', '=', filter_var($email, FILTER_SANITIZE_EMAIL))->get();

        $user = $result->first();

		if($user !== null && password_verify($password, $user->password)) {
            self::loadProfile($user->id);
            return true;
        }

        return false;
    }

    private static function loadProfile($uid) {
        $app = Slim::getInstance();

        $user = User::find($uid);

        if($user !== null) {
            $_SESSION['profile'] = array(
                'id' => $user->id,
                'email' => $user->email,
                'role' => $user->role,
                'ip' => $app->request->getIp()
            );
        }
    }

    public static function checkAccessRights($required) {
        if(isset($_SESSION['profile']) && $_SESSION['profile']['role'] < $required)
            throw new AuthException("Vous n'avez pas la permission d'accèder à ce contenu");
    }

    public static function createUser($email, $password) {
        if(strlen($password) > 7 && strlen($password) < 31 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $user = new User();
            $user->email = $email;
            $user->password = password_hash($password, PASSWORD_DEFAULT);
            $user->created_at = new \DateTime();
            $user->role = self::USER;

            return ($user->save()) ? $user : null;
        }

        return null;
    }

    public static function logout() {
        unset($_SESSION['profile']);
    }

    public static function isAuthenticated() {
        return isset($_SESSION['profile']);
    }

    public static function getUser() {
        return (self::isAuthenticated()) ? User::find($_SESSION['profile']['id']) : null;
    }

    public static function getId() {
        return (isset($_SESSION['profile'])) ? $_SESSION['profile']['id'] : '';
    }

    public static function getEmail() {
        return (isset($_SESSION['profile'])) ? $_SESSION['profile']['email'] : '';
    }

    public static function getIp() {
        return (isset($_SESSION['profile'])) ? $_SESSION['profile']['ip'] : '';
    }

}
