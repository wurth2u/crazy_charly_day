<div class="container">
    <p class="alert alert-success">Votre compte a été créé avec succès ! Vous pouvez maintenant <a href="<?php echo \Slim\Slim::getInstance()->urlFor('login'); ?>" class="alert-link">vous connecter</a>.</p>
</div>
