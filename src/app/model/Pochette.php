<?php
/**
 * Created by PhpStorm.
 * User: alexis
 * Date: 11/02/16
 * Time: 10:27
 */

namespace app\model;


use Illuminate\Database\Eloquent\Model;

class Pochette extends Model {

    protected $table = 'pochette';

    protected $primaryKey = 'id';

    public $timestamps = false;

    public function prestations() {
        return $this->belongsToMany('app\model\Prestation');
    }

}