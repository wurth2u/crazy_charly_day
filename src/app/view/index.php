<div class="container">
	<div class="page-header">
		<h1>Accueil</h1>
	</div>

	<h3 class="category-heading text-center">Les meilleures prestations</h3>

	<div class="row prest-list">
		<div class="col-lg-12">
			<?php
			$i = 1;
			foreach($prestations as $prestation) {
				if($i % 3 == 0)
					echo '<div class="row">';

				$moy = $prestation->moyenne();
				$note = '';
				$numNote = 1;
				for($j = 0 ; $j < $moy ; $j++) {
					$note .= '<a href="' . \Slim\Slim::getInstance()->urlFor('noter_presta', array('id' => $prestation->id, 'note' => $numNote)) . '"><span class="glyphicon glyphicon-star"></span></a> ';
					$numNote++;
				}

				for($j = 0 ; $j < 5 - $moy ; $j++) {
					$note .= '<a href="' . \Slim\Slim::getInstance()->urlFor('noter_presta', array('id' => $prestation->id, 'note' => $numNote)) . '"><span class="glyphicon glyphicon-star-empty"></span></a> ';
					$numNote++;
				}

				echo '
                    <div class="col-lg-4">
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 card">
                                <div class="row">
                                    <a href="' . \Slim\Slim::getInstance()->urlFor('view_prestation', array('id' => $prestation->id)) . '">
                                        <img src="'.\Slim\Slim::getInstance()->urlFor('home') . 'img/' . $prestation->img .'" alt="Image prestation">
                                    </a>
                                </div>
                                <div class="card-bottom">
                                    <h3><a href="' . \Slim\Slim::getInstance()->urlFor('view_prestation', array('id' => $prestation->id)) . '">'.$prestation->nom . '</a></h3>
                                    ' . $note . '
                                    <p>' . $prestation->prix . ' €</p>
                                    <p class="text-ellipsis prest-desc">' . $prestation->descr . '</p>
                                    <a href="' . \Slim\Slim::getInstance()->urlFor('add_prestation', array('id' => $prestation->id)) . '" class="btn btn-lg card-btn">Ajouter à la pochette</a>
                                </div>
                            </div>
                        </div>
                    </div>
                ';

				if($i % 3 == 0)
					echo '</div>';

				$i++;
			}
			?>
		</div>
	</div>
</div>
