<?php

namespace app\controller;

use app\utils\Authentication;
use Illuminate\Database\QueryException;
use Slim\Slim;

class AuthController {

    public function login() {
        $app = Slim::getInstance();

        if(Authentication::isAuthenticated())
            $app->redirectTo('home');

        $app->render('auth/login.php');
    }

    public function loginVerify() {
        $app = Slim::getInstance();

        if(Authentication::isAuthenticated())
            $app->redirectTo('home');

        $r = $app->request;
        if($r->post('email') === null || $r->post('password') === null) {
            $app->flash('auth_error', 'Il manque des champs');
            $app->redirectTo('login');
        }

        if(Authentication::authenticate($r->post('email'), $r->post('password')))
            $app->redirectTo('home');

        $app->flash('auth_error', 'L\'adresse email ou le mot de passe sont incorrects');
        $app->redirectTo('login');
    }

    public function registerVerify() {
        $app = Slim::getInstance();

        if(Authentication::isAuthenticated())
            $app->redirectTo('home');

        $email = $app->request->post('email');
        $password = $app->request->post('password');
        $passVerif = $app->request->post('password-verify');

        $fields = array(
            $email,
            $password,
            $passVerif
        );

        // Si des champs n'ont pas été envoyés
        foreach($fields as $field) {
            if($field === null) {
                $app->flash('auth_error', 'Il manque des champs');
                $app->redirectTo('login');
            }
        }

        // Si le mot de passe ne correspond pas au mot de passe de confirmation
        if($password != $passVerif) {
            $app->flash('auth_error', 'Les mots de passe ne correspondent pas');
            $app->redirectTo('login');
        }

        // Création de l'utilisateur
        try {
            $user = Authentication::createUser($email, $password);
        } catch(QueryException $e) {
            $app->flash('auth_error', 'Cette adresse email est déjà utilisée !');
            $app->redirectTo('login');
        }

        // Si l'utilisateur a bien été créé
        if($user !== null) {
            $app->redirectTo('register_confirm');
        } else {
            $app->flash('auth_error', 'Une erreur inconnue s\'est produite lors de l\'inscription...');
            $app->redirectTo('login');
        }
    }

    public function registerConfirm() {
        $app = Slim::getInstance();

        if(Authentication::isAuthenticated())
            $app->redirectTo('home');

        $app->render('auth/register_confirm.php');
    }

    public function logout() {
        Authentication::logout();
        Slim::getInstance()->redirectTo('home');
    }

}