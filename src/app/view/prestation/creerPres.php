<div class="creerPres">
    <div class="page-header">
        <h1>Cr&eacute;er une prestation</h1>
        <div id="content">
            <form id="f1" method="post" action="<?php echo \Slim\Slim::getInstance()->urlFor('catalogue');?>">

                <table id="tab">
                    <tr><td>Nom:&nbsp;</td><td><input type="text" name="nom" style="width:250px"/></td></tr>
                    <tr><td>Description:&nbsp;</td><td><textarea name="crtpres-desc" id="crtpres-desc"></textarea></td></tr>
                    <tr><td>Type:&nbsp;</td><td><select name = "choice" style="width:250px">
                        <option value ="999999">Type de Prestation</option>
                        <option value ="1">Attention</option>
                        <option value ="2">Activite</option>
                        <option value ="3">Restauration</option>
                        <option value ="4">Hebergement</option>
                    </select></td></tr>
                    <tr><td>Prix:&nbsp;</td><td><input type="text" name="prix" style="width:50px"/></td></tr>
                    <tr><td>Image:&nbsp;</td><td><input type="file" name="photo"/></td></tr>
                </table>
                <input type="submit" value="Creer">
            </form>
        </div>
    </div>
</div>
