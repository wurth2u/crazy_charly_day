<?php
/**
 * Created by PhpStorm.
 * User: alexis
 * Date: 11/02/16
 * Time: 09:55
 */

namespace app\controller;


use app\model\Prestation;
use Slim\Slim;

class PrestationController {

    public function view($id) {
        $app = Slim::getInstance();

        $prestation = Prestation::find($id);

        if($prestation === null) {
            $app->render("header.php");
            $app->render('error.php', array('message' => 'Cette prestation n\'existe pas !'));
            return;
        }
        $app->render("header.php");
        $app->render('prestation/view.php', array('prestation' => $prestation));
    }

    public function listAll(){
        $app = Slim::getInstance();
        $tri = $app->request->get('tri');
        if (isset($tri)) {
            if ($tri == 'nomAsc') {
                $liste = Prestation::orderBy('nom', 'asc') ->get();
            }
            if ($tri == 'nomDesc') {
                $liste = Prestation::orderBy('nom', 'desc') ->get();
            }
            if ($tri == 'prixAsc') {
                $liste = Prestation::orderBy('prix', 'asc') ->get();
            }
            if ($tri == 'prixDesc') {
                $liste = Prestation::orderBy('prix', 'desc') ->get();
            }
        }
        else {
            $liste = Prestation::all();
        }
        $app->render("header.php");
        $app->render('prestation/prestations.php', array('liste' => $liste));

    }

    public function validationFormulaire() {
        $app = Slim::getInstance();
        $type = $app->request->post()['choice'];
        $liste = Prestation::all();
        $tri=array();
        if($type == 0){
            $tri = $liste;
        }
        else {
            foreach ($liste as $ligne) {
                if ($ligne->type == $type) {
                    array_push($tri, $ligne);
                }
            }
        }
        $app->render("header.php");
        $app->render('prestation/prestations.php', array('liste' => $tri));

    }

    public function creerPrestattion(){
        $app = Slim::getInstance();
        $app->render("header.php");
        $app->render('prestation/creerPres.php',array());

    }

    public function envoyerPres(){
        $app = Slim::getInstance();

        $nom = $app->request->post()['nom'];

        $desc = $app->request->post()['crtpres-desc'];

        $img = $app->request->post()['photo'];

        $prix = $app->request->post()['prix'];

        $pres = new Prestation();

        $pres->nom = filter_var($nom, FILTER_SANITIZE_SPECIAL_CHARS);
        $pres->descr = filter_var($desc, FILTER_SANITIZE_SPECIAL_CHARS);
        $pres->prix = filter_var($prix, FILTER_SANITIZE_SPECIAL_CHARS);
        $pres->img = $img;
        imagepng($img,"/web/img");
        $pres->save();


    }

}