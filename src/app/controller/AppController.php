<?php

namespace app\controller;

use app\model\Type;
use Slim\Slim;

class AppController {

    public function index() {
        $app = Slim::getInstance();

        $types = Type::all();
        $bestPrestList = array();

        foreach($types as $type) {
            $bestPrest = null;
            $bestMoy = 0;
            foreach($type->prestations as $prestation) {
                $moy = $prestation->moyenne();
                if($moy >= $bestMoy) {
                    $bestPrest = $prestation;
                    $bestMoy = $moy;
                }
            }
            if($bestPrest !== null)
                $bestPrestList[] = $bestPrest;
        }

        $app->render('header.php');
        $app->render('index.php', array('prestations' => $bestPrestList));
    }


}
