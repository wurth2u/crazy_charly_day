<?php
/**
 * Created by PhpStorm.
 * User: alexis
 * Date: 11/02/16
 * Time: 14:59
 */

namespace app\model;


use Illuminate\Database\Eloquent\Model;

class Type extends Model{

    protected $table = 'type';

    protected $primaryKey = 'id';

    public $timestamps = false;

    public function prestations() {
        return $this->hasMany('app\model\Prestation', 'type');
    }

}