<?php

namespace app\controller;

use app\model\Notation;
use app\model\Pochette;
use app\model\Prestation;
use Slim\Slim;

class NotationController {

    public function noter($id,$note) {
        $app = Slim::getInstance();

        if(!isset($_SESSION['pochette'])) {
            $app->flash('message','<p class="alert alert-danger">Vous devez d\'abord créer une pochette avant de noter les prestations !</p>');
            $app->redirectTo('pochette_form');
        }

        $pochette = Pochette::find($_SESSION['pochette']['id']);
        $prestation = Prestation::find($id);

        if($pochette === null) {
            $app->flash('message', '<p class="alert alert-danger">Cette pochette n\'a pas été créée de façon correcte !</p>');
            $app->redirectTo('pochette_form');
        }

        if($prestation === null) {
            $app->flash('message', '<p class="alert alert-danger">Cette prestation n\'existe pas !</p>');
            $app->redirectTo('catalogue');
        }

        if($note > 5 || $note < 1) {
            $app->flash('message', '<p class="alert alert-danger">La note que vous voulez donner n\'est pas correcte</p>');
            $app->redirectTo('catalogue');
        }

        $notes = Notation::all();

        foreach($notes as $val) {
            if ($val->prestation_id == $id && $val->email_not == $pochette->email) {
                $app->flash('message', '<p class="alert alert-danger">Vous avez déjà noté cette prestation !</p>');
                $app->redirectTo('catalogue');
            }
        }

        $notation = new Notation();

        $notation->prestation_id = $id;
        $notation->email_not = $pochette->email;
        $notation->note = $note;

        $notation->save();

        $app->flash('message','<p class="alert alert-success">Merci d\'avoir noté la prestation !</p>');
        $app->redirectTo('catalogue');
    }

}