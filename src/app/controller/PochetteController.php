<?php
/**
 * Created by PhpStorm.
 * User: alexis
 * Date: 11/02/16
 * Time: 10:28
 */

namespace app\controller;

use app\model\Cagnotte;
use app\model\Pochette;
use app\model\Prestation;
use Slim\Slim;

class PochetteController {

    public function showFormulaire() {
        $app = Slim::getInstance();

        $app->render('header.php');
        $app->render('pochette/formulaire.php');
    }

    public function addPrestation($id) {
        $app = Slim::getInstance();

        if(!isset($_SESSION['pochette'])) {
            $app->flash('message', '<p class="alert alert-danger">Vous devez d\'abord créer une pochette !</p>');
            $app->redirectTo('pochette_form');
        }

        $pochette = Pochette::find($_SESSION['pochette']['id']);
        $prestation = Prestation::find($id);

        if($pochette === null) {
            $app->flash('message', '<p class="alert alert-danger">Cette pochette n\'existe pas !</p>');
            $app->redirectTo('pochette_form');
        }

        if($prestation === null) {
            $app->flash('message', '<p class="alert alert-danger">Cette prestation n\'existe pas !</p>');
            $app->redirectTo('catalogue');
        }

        foreach($pochette->prestations as $p) {
            if($p->id == $id) {
                $app->flash('message', '<p class="alert alert-danger">Cette prestation est déjà dans votre pochette !</p>');
                $app->redirectTo('catalogue');
            }
        }

        $pochette->prestations()->attach($prestation->id);

        $app->flash('message', '<p class="alert alert-success">La prestation a bien été ajoutée à la pochette !</p>');
        $app->redirectTo('catalogue');
    }

    public function removePrestation($id) {
        $app = Slim::getInstance();

        if(!isset($_SESSION['pochette'])) {
            $app->flash('message', '<p class="alert alert-danger">Vous devez d\'abord créer une pochette !</p>');
            $app->redirectTo('pochette_form');
        }

        $pochette = Pochette::find($_SESSION['pochette']['id']);
        $prestation = Prestation::find($id);

        if($pochette === null) {
            $app->flash('message', '<p class="alert alert-danger">Cette pochette n\'existe pas !</p>');
            $app->redirectTo('pochette_form');
        }

        if($prestation === null) {
            $app->flash('message', '<p class="alert alert-danger">Cette prestation n\'existe pas !</p>');
            $app->redirectTo('catalogue');
        }

        $notRemoved = false;
        foreach($pochette->prestations as $p) {
            if($p->id == $id)
                $notRemoved = true;
        }

        if(!$notRemoved) {
            $app->flash('message', '<p class="alert alert-danger">Cette prestation n\'est pas dans votre pochette ou a déjà été supprimée !</p>');
            $app->redirectTo('view_pochette');
        }

        $pochette->prestations()->detach($prestation->id);

        $app->flash('message', '<p class="alert alert-success">La prestation a bien été supprimée de la pochette !</p>');
        $app->redirectTo('view_pochette');
    }

    public function valider() {
        $app = Slim::getInstance();

        if(!isset($_SESSION['pochette'])) {
            $app->flash('message', '<p class="alert alert-danger">Vous devez d\'abord créer une pochette !</p>');
            $app->redirectTo('pochette_form');
        }

        $pochette = Pochette::find($_SESSION['pochette']['id']);

        if($pochette === null) {
            $app->flash('message', '<p class="alert alert-danger">Cette pochette n\'existe pas !</p>');
            $app->redirectTo('pochette_form');
        }

        $types = array();
        foreach($pochette->prestations as $prestation) {
            $double = false;
            foreach($types as $type) {
                if($type == $prestation->getType->nom)
                    $double = true;
            }

            if(!$double)
                $types[] = $prestation->getType->nom;
        }

        $nbTypes = count($types);
        if($nbTypes < 3) {
            $app->flash('message', '<p class="alert alert-danger">Votre pochette ne contient que ' . $nbTypes . ' types de prestations ! Elle doit en contenir au moins 3.</p>');
            $app->redirectTo('catalogue');
        }

        $pochette->token = uniqid(substr(md5($pochette->id), 0, 10));
        $pochette->save();

        $tok = $pochette->token;

        $cagnotte = new Cagnotte();

        $totalPrix = 0;
        foreach($pochette->prestations as $prest) {
            $totalPrix = $totalPrix + $prest->prix;
        }

        $cagnotte->objectif = $totalPrix;
        $cagnotte->id_pochette = $pochette->id;
        $cagnotte->cagnotte = 0;

        $cagnotte->save();

        $app->flash('message', '<p class="alert alert-success">Votre pochette a bien été validée ! Vous pouvez la partager avec vos amis en leur envoyant le lien ci-dessous :<p>'.$app->urlFor('cagnotte',array('token'=>$tok)).'</p></p>');

        $app->redirectTo('cagnotte', array('token'=>$tok));
    }

    public function validationFormulaire() {
        $app = Slim::getInstance();

        $name = $app->request->post()['nom'];

        $msg = $app->request->post()['message'];

        $pass = $app->request->post()['mdp'];

        $mail = $app->request->post()['email'];

        $newMdp = sha1($pass);
        $poch = new Pochette();

        $poch->nom = filter_var($name, FILTER_SANITIZE_SPECIAL_CHARS);
        $poch->message = filter_var($msg, FILTER_SANITIZE_SPECIAL_CHARS);
        $poch->email = filter_var($mail, FILTER_SANITIZE_EMAIL);
        $poch->password = $newMdp;

        if($poch->save()) {
            $_SESSION['pochette'] = array(
                'nom' => $poch->nom,
                'message' => $poch->message,
                'email' => $poch->email,
                'id' => $poch->id
            );

            $app->redirectTo('view_pochette');
        } else {
            $app->flash('message', '<p class="alert alert-danger">Une erreur s\'est produite. Votre pochette n\'a pas pu etre créée.</p>');
            $app->redirectTo('pochette_form');
        }
    }

    public function view() {
        $app = Slim::getInstance();

        if(!isset($_SESSION['pochette'])) {
            $app->flash('message', '<p class="alert alert-danger">Vous devez d\'abord créer une pochette !</p>');
            $app->redirectTo('pochette_form');
        }

        $pochette = Pochette::find($_SESSION['pochette']['id']);

        if($pochette === null) {
            $app->flash('message', '<p class="alert alert-danger">Cette pochette n\'existe pas !</p>');
            $app->redirectTo('pochette_form');
        }

        $app->render('header.php');
        $app->render('pochette/view.php', array('pochette' => $pochette));
    }

    public function viewById($id) {
        $app = Slim::getInstance();

        $pochette = Pochette::find($id);

        if($pochette === null) {
            $app->flash('message', '<p class="alert alert-danger">Cette pochette n\'existe pas !</p>');
            $app->redirectTo('pochette_form');
        }

        $app->render('pochette/view_by_id.php', array('pochette' => $pochette));
    }

    public function gerer($id) {
        $app = Slim::getInstance();

        $pochette = Pochette::find($id);

        if($pochette === null) {
            $app->flash('message', '<p class="alert alert-danger">Cette pochette n\'existe pas !</p>');
            $app->redirectTo('pochette_form');
        }

        $email = $app->request->post('email');
        $password = $app->request->post('password');

        if($email === null || $password === null) {
            $app->flash('message', '<p class="alert alert-danger">Il manque des champs !</p>');
            $app->redirectTo('view_pochette_by_id', array('id' => $id));
        }

        if($pochette->email !== $email || $pochette->password !== sha1($password)) {
            $app->flash('message', '<p class="alert alert-danger">L\'adresse email ou le mot de passe sont incorrects...</p>');
            $app->redirectTo('view_pochette_by_id', array('id' => $id));
        }

        $_SESSION['pochette'] = array(
            'nom' => $pochette->nom,
            'message' => $pochette->message,
            'email' => $pochette->email,
            'id' => $pochette->id
        );

        $app->redirectTo('view_pochette');
    }

    public function delete() {
        $app = Slim::getInstance();

        if(!isset($_SESSION['pochette'])) {
            $app->flash('message', '<p class="alert alert-danger">Vous devez d\'abord créer une pochette !</p>');
            $app->redirectTo('pochette_form');
        }

        $pochette = Pochette::find($_SESSION['pochette']['id']);

        if($pochette === null) {
            $app->flash('message', '<p class="alert alert-danger">Cette pochette n\'existe pas !</p>');
            $app->redirectTo('pochette_form');
        }

        $pochette->prestations()->detach();
        $pochette->delete();

        unset($_SESSION['pochette']);

        $app->flash('message', '<p class="alert alert-success">La pochette a bien été supprimée !</p>');
        $app->redirectTo('home');
    }

}