<div class="cagnotte">
    <div class="url">
        <p>
            <?php  echo 'vmathias.fr'.\Slim\Slim::getInstance()->urlFor('cagnotte', array('token'=>$token)); ?>
        </p>
        <p>Montant de la cagnotte à atteindre : <?php
            $pochette = \app\model\Pochette::where('token','=',$token);
            $cagnotte = \app\model\Cagnotte::where('id_pochette','=',$pochette->first()->id);
            echo $cagnotte->first()->objectif;
            ?> €</p>
        <p>Montant actuel : <?php
            $pochette = \app\model\Pochette::where('token','=',$token);
            $cagnotte = \app\model\Cagnotte::where('id_pochette','=',$pochette->first()->id);
            echo $cagnotte->first()->cagnotte;
            ?> €</p>
        <form method="post" action="<?php  echo \Slim\Slim::getInstance()->urlFor('cagnotte_ajout',array('token'=>$token)); ?>">
            <label>Votre montant :</label>
            <input type="number" name="montant">
            <button type="submit">Valider montant</button>
        </form>
    </div>
</div>