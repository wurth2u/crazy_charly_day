<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model;

class Cagnotte extends Model {

	protected $table = 'cagnotte';

	protected $primaryKey = 'id';

	public $timestamps = false;

	public function cagnottes() {
		return $this->belongsTo('app\model\Pochette');
	}

}