<div class="container">
    <div class="page-header">
        <h1>Créer une pochette</h1>
    </div>
    <form method="post" action="<?php echo \Slim\Slim::getInstance()->urlFor('pochette_ajout_form'); ?>">
        <table>
            <tr>
                <td><label>Nom de la pochette :</label></td>
                <td><input type="text" name="nom"></td>
            </tr>
            <tr>
                <td><label>Message addressé au destinataire :</label></td>
                <td><input type="text" name="message"></td>
            </tr>
            <tr>
                <td><label>Adresse mail :</label></td>
                <td><input type="email" name="email"></td>
            </tr>
            <tr>
                <td><label>Mot de passe :</label></td>
                <td><input type="password" name="mdp"></td>
            </tr>
            <tr><td><button type="submit">Valider</button> </td></tr>
        </table>
    </form>
</div>