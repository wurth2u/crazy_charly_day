<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Crazy Pochette Surprise</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo \Slim\Slim::getInstance()->urlFor('home'); ?>css/bootstrap-datepicker3.standalone.min.css">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo \Slim\Slim::getInstance()->urlFor('home'); ?>css/main.css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo \Slim\Slim::getInstance()->urlFor('home'); ?>">
                <img alt="Crazy Charly Day" src="" id="logo">
            </a>
        </div>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="<?php echo \Slim\Slim::getInstance()->urlFor('catalogue'); ?>">Catalogue</a></li>
                <?php
                    if(isset($_SESSION['pochette']))
                        echo '<li><a href="' . \Slim\Slim::getInstance()->urlFor('view_pochette') . '">Ma pochette</a></li>';
                    else
                        echo '<li><a href="' . \Slim\Slim::getInstance()->urlFor('pochette_form') . '">Créer une pochette</a></li>';
                ?>
            </ul>
        </div>
    </div>
</nav>