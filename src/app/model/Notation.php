<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model;

class Notation extends Model {

	protected $table = 'notation';

	protected $primaryKey = 'id';

	public $timestamps = false;

	public function notations() {
		return $this->belongsToMany('app\model\Prestation');
	}

}