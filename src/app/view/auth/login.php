<div class="container">
    <?php echo (isset($flash['auth_error'])) ? '<p class="alert alert-danger"> <span class="glyphicon glyphicon-warning-sign"></span> ' . $flash['auth_error'] . '</p>' : ''; ?>
    <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Connexion</h3>
                    </div>
                    <div class="panel-body">
                        <form action="<?php echo \Slim\Slim::getInstance()->urlFor('login_verify'); ?>" method="POST">
                            <div class="row">
                                <div class="form-group col-lg-10 col-lg-offset-1">
                                    <label for="email">Adresse email</label>
                                    <input type="email" name="email" id="email" class="form-control" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-10 col-lg-offset-1">
                                    <label for="password">Mot de passe</label>
                                    <input type="password" name="password" id="password" class="form-control" required>
                                </div>
                            </div>
                            <input type="submit" value="Connexion" class="btn btn-primary col-lg-offset-1">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Inscription</h3>
                    </div>
                    <div class="panel-body">
                        <form action="<?php echo \Slim\Slim::getInstance()->urlFor('register_verify'); ?>" method="POST">
                            <div class="row">
                                <div class="form-group col-lg-10 col-lg-offset-1">
                                    <label for="prenom">Prénom</label>
                                    <input type="text" name="prenom" id="prenom" class="form-control">
                                </div>
                                <div class="form-group col-lg-10 col-lg-offset-1">
                                    <label for="nom">Nom</label>
                                    <input type="text" name="nom" id="nom" class="form-control">
                                </div>
                                <div class="form-group col-lg-10 col-lg-offset-1">
                                    <label for="adresse">Adresse</label>
                                    <input type="text" name="adresse" id="adresse" class="form-control">
                                </div>
                                <div class="col-lg-5 col-lg-offset-1">
                                    <div class="row">
                                        <div class="form-group col-lg-12">
                                            <label for="codepostal">Code postal</label>
                                            <input type="number" name="codepostal" id="codepostal" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="row">
                                        <div class="form-group col-lg-12">
                                            <label for="ville">Ville</label>
                                            <input type="text" name="ville" id="ville" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-lg-offset-1">
                                    <div class="row">
                                        <div class="form-group col-lg-12">
                                            <label for="region">Région</label>
                                            <input type="text" name="region" id="region" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="row">
                                        <div class="form-group col-lg-12">
                                            <label for="departement">Département</label>
                                            <input type="text" name="departement" id="departement" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-lg-10 col-lg-offset-1">
                                    <label for="email">Adresse email *</label>
                                    <input type="email" name="email" id="email" class="form-control" required>
                                </div>
                                <div class="form-group col-lg-10 col-lg-offset-1">
                                    <label for="password">Mot de passe *</label>
                                    <input type="password" name="password" id="password" class="form-control" required>
                                </div>
                                <div class="form-group col-lg-10 col-lg-offset-1">
                                    <label for="password-verify">Confirmer mot de passe *</label>
                                    <input type="password" name="password-verify" id="password-verify" class="form-control" required>
                                </div>
                            </div>
                            <input type="submit" value="Inscription" class="btn btn-primary col-lg-offset-1">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
