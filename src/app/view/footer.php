        <footer></footer>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script src="<?php echo \Slim\Slim::getInstance()->urlFor('home'); ?>js/bootstrap-datepicker.min.js"></script>
        <script src="<?php echo \Slim\Slim::getInstance()->urlFor('home'); ?>js/bootstrap-datepicker.fr.min.js"></script>
        <script>
                $(document).ready(function() {
                        $('.day-date input').datepicker({
                                language : 'fr'
                        });

                        $('.card-bottom').click(function() {
                                $(this).children('p.prest-desc').slideToggle();
                        });

                        $('.glyphicon-star-empty').hover(function() {
                                $(this).attr('class', 'glyphicon glyphicon-star');
                        }, function() {
                                    $(this).attr('class', 'glyphicon glyphicon-star-empty');
                            }
                        );
                });
        </script>
        </body>
</html>
