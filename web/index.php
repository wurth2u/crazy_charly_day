<?php

require_once '../vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use Slim\Slim;
use app\controller\AppController;
use app\controller\PrestationController;
use app\controller\PochetteController;
use app\controller\NotationController;
use app\controller\CagnotteController;

/* ---------------------------------------- */

session_start();

$params = array();

if(is_file('../src/conf/db.conf.ini'))
	$params = parse_ini_file('../src/conf/db.conf.ini');

$db = new DB();

$db->addConnection($params);

$db->setAsGlobal();
$db->bootEloquent();

/* ---------------------------------------- */

$app = new Slim();
$app->config('templates.path', '../src/app/view');

$app->get('/', function() {
	$c = new AppController();
	$c->index();
})->name('home');

/*$app->get('/login', function() {
	$c = new AuthController();
	$c->login();
})->name('login');

$app->get('/login/verify', function() {
	$c = new AuthController();
	$c->loginVerify();
})->name('login_verify');

$app->get('/register', function() {
	$c = new AuthController();
	$c->registerVerify();
})->name('register_verify');*/

$app->get('/prestation/:id', function($id) {
	$c = new PrestationController();
	$c->view($id);
})->name('view_prestation');

$app->get('/prestation/:id/ajouter', function($id) {
	$c = new PochetteController();
	$c->addPrestation($id);
})->name('add_prestation');

$app->get('/prestation/:id/supprimer', function($id) {
	$c = new PochetteController();
	$c->removePrestation($id);
})->name('remove_prestation');

$app->get('/catalogue', function() {
	$c = new PrestationController();
	$c->listAll();
})->name('catalogue');

$app->post('/catalogue', function(){
	$c = new PrestationController();
	$c->validationFormulaire();
})->name('catalogue_tri');

$app->get('/pochette/valider', function() {
	$c = new PochetteController();
	$c->valider();
})->name('valider');

$app->get('/pochette/creer', function() {
	$c = new PochetteController();
	$c->showFormulaire();
})->name('pochette_form');

$app->get('/pochette/supprimer', function() {
	$c = new PochetteController();
	$c->delete();
})->name('delete_pochette');

$app->post('/pochette/save', function() {
	$c = new PochetteController();
	$c->validationFormulaire();
})->name('pochette_ajout_form');

$app->get('/pochette/voir', function() {
	$c = new PochetteController();
	$c->view();
})->name('view_pochette');

$app->get('/pochette/:id', function($id) {
	$c = new PochetteController();
	$c->viewById($id);
})->name('view_pochette_by_id');

$app->post('/pochette/:id/gerer', function($id) {
	$c = new PochetteController();
	$c->gerer($id);
})->name('gerer_pochette');

$app->get('/creerPres',function(){
	$c = new PrestationController();
	$c->creerPrestattion();
})->name('creerPres');
$app->post('/creerPres', function() {
	$c = new PrestationController();
	$c->envoyerPres();
})->name('envoyer_pres');


$app->get('/prestation/:id/noter/:note',function($id,$note) {
	$c = new NotationController();
	$c->noter($id,$note);
})->name('noter_presta');

$app->get('/cagnotte/:token', function($token) {
	$c = new CagnotteController();
	$c->afficherCagnotte($token);
})->name('cagnotte');

$app->post('/cagnotte/ajouter/:token',function ($token) {
	$c = new CagnotteController();
	$c->addMontant($token);
})->name('cagnotte_ajout');


/* ---------------------------------------- */

$app->run();
$app->render('footer.php');
