<?php

namespace app\controller;

use app\model\Type;
use Slim\Slim;

class CagnotteController {

    public function afficherCagnotte($token) {
        $app = Slim::getInstance();

        $app->render('header.php');
        $app->render('cagnotte/cagnotte.php',array('token'=>$token));
    }

    public function addMontant($token) {
        $app = Slim::getInstance();

        $montant = $app->request->post('montant');

        $pochette = \app\model\Pochette::where('token','=',$token);
        $cagnotte = \app\model\Cagnotte::where('id_pochette','=',$pochette->first()->id);

        $c = $cagnotte->first();

        $c->cagnotte += $montant;

        $c->save();

        $app->redirectTo('cagnotte',array('token'=>$token));
    }


}