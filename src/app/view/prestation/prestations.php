<div class="container">
    <div class="page-header">
        <h1>Liste des prestations</h1>
    </div>
    <?php echo $flash['message']; ?>

    <div class="row">
        <div class="col-lg-4">
            <form id="f1" method="post" class="form-inline">
                <div class="form-group">
                    <label for="tri">Trier par :</label>
                    <select name="choice" class="form-control" id="tri">
                        <option value="0">Afficher Tout</option>
                        <option value="1">Attention</option>
                        <option value="2">Activité</option>
                        <option value="3">Restauration</option>
                        <option value="4">Hébergement</option>
                    </select>
                </div>
                <input type="submit" value="Trier" class="btn btn-primary">
            </form>
        </div>
        <div class="col-lg-4">
            <div class="btn-group" role="group">
                <a href="<?php echo \Slim\Slim::getInstance()->urlFor('catalogue') . '?tri=nomAsc'; ?>" class="btn btn-default">Nom asc</a>
                <a href="<?php echo \Slim\Slim::getInstance()->urlFor('catalogue') . '?tri=nomDesc'; ?>" class="btn btn-default">Nom desc</a>
            </div>
            <div class="btn-group" role="group">
                <a href="<?php echo \Slim\Slim::getInstance()->urlFor('catalogue') . '?tri=prixAsc'; ?>" class="btn btn-default">Prix asc</a>
                <a href="<?php echo \Slim\Slim::getInstance()->urlFor('catalogue') . '?tri=prixDesc'; ?>" class="btn btn-default">Prix desc</a>
            </div>
        </div>
        <div class="col-lg-2 col-lg-offset-2">
            <?php
                $buttonpath = \Slim\Slim::getInstance()->urlFor('home').'creerPres';
                echo '<a href="' . $buttonpath . '" class="btn btn-default">Créer une prestation</a>';
            ?>
        </div>
    </div>

    <div class="row prest-list">
        <div class="col-lg-12">
        <?php
            $i = 1;
            foreach($liste as $ligne) {
                $img = $ligne->img;
                $path = '<img src="'.\Slim\Slim::getInstance()->urlFor('home') . 'img/' . $img .'">';

                if($i % 3 == 0)
                    echo '<div class="row">';

                $moy = $ligne->moyenne();
                $note = '';
                $numNote = 1;
                for($j = 0 ; $j < $moy ; $j++) {
                    $note .= '<a href="' . \Slim\Slim::getInstance()->urlFor('noter_presta', array('id' => $ligne->id, 'note' => $numNote)) . '"><span class="glyphicon glyphicon-star"></span></a> ';
                    $numNote++;
                }

                for($j = 0 ; $j < 5 - $moy ; $j++) {
                    $note .= '<a href="' . \Slim\Slim::getInstance()->urlFor('noter_presta', array('id' => $ligne->id, 'note' => $numNote)) . '"><span class="glyphicon glyphicon-star-empty"></span></a> ';
                    $numNote++;
                }

                echo '
                    <div class="col-lg-4">
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 card">
                                <div class="row">
                                    <a href="' . \Slim\Slim::getInstance()->urlFor('view_prestation', array('id' => $ligne->id)) . '">
                                        <img src="'.\Slim\Slim::getInstance()->urlFor('home') . 'img/' . $img .'" alt="Image prestation">
                                    </a>
                                </div>
                                <div class="card-bottom">
                                    <h3><a href="' . \Slim\Slim::getInstance()->urlFor('view_prestation', array('id' => $ligne->id)) . '">'.$ligne->nom . '</a></h3>
                                    ' . $note . '
                                    <p>' . $ligne->prix . ' €</p>
                                    <p class="text-ellipsis prest-desc">' . $ligne->descr . '</p>
                                    <a href="' . \Slim\Slim::getInstance()->urlFor('add_prestation', array('id' => $ligne->id)) . '" class="btn btn-lg card-btn">Ajouter à la pochette</a>
                                </div>
                            </div>
                        </div>
                    </div>
                ';

                if($i % 3 == 0)
                    echo '</div>';

                $i++;
            }
        ?>
        </div>
    </div>
</div>
