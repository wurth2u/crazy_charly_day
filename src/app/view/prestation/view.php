<?php
    $moy = $prestation->moyenne();
    $note = '';
    $numNote = 1;
    for($j = 0 ; $j < $moy ; $j++) {
        $note .= '<a href="' . \Slim\Slim::getInstance()->urlFor('noter_presta', array('id' => $prestation->id, 'note' => $numNote)) . '"><span class="glyphicon glyphicon-star"></span></a> ';
        $numNote++;
    }

    for($j = 0 ; $j < 5 - $moy ; $j++) {
        $note .= '<a href="' . \Slim\Slim::getInstance()->urlFor('noter_presta', array('id' => $prestation->id, 'note' => $numNote)) . '"><span class="glyphicon glyphicon-star-empty"></span></a> ';
        $numNote++;
    }
?>

<div class="container">
    <div class="row">
        <div class="col-lg-12 card">
            <div class="prest-img">
                <img src="<?php echo \Slim\Slim::getInstance()->urlFor('home') . 'img/' . $prestation->img; ?>" alt="Image prestation" class="center-block">
            </div>
            <div class="card-bottom">
                <h1 class="prest-title"><?php echo $prestation->nom; ?><small class="pull-right"><?php echo $prestation->prix . ' €'; ?></small></h1>
                <?php echo $note; ?><br>
                <span class="label label-default"><?php echo $prestation->getType->nom; ?></span>
                <p class="prest-descr"><?php echo $prestation->descr; ?></p>
                <a href="<?php echo \Slim\Slim::getInstance()->urlFor('add_prestation', array('id' => $prestation->id)); ?>" class="btn btn-lg card-btn">Ajouter à la pochette</a>
            </div>
        </div>
    </div>
</div>
