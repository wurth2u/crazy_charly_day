<div class="container">
    <div class="page-header">
        <h1>Pochette</h1>
    </div>
    <?php echo $flash['message']; ?>
    <h3>Pochette</h3>
    <ul>
        <?php
            echo '
                <li>Nom : ' . $pochette->nom . '</li>
                <li>Message : ' . $pochette->message . '</li>
            ';
        ?>
    </ul>
    <h3>Prestations</h3>
    <ul>
        <?php
            foreach($pochette->prestations as $prestation) {
                echo '
                    <li>
                        <a href="' . \Slim\Slim::getInstance()->urlFor('view_prestation', array('id' => $prestation->id)) . '">' . $prestation->nom . '</a>
                    </li>
                ';
            }
        ?>
    </ul>
    <div class="row">
        <div class="col-lg-4">
            <form action="<?php echo \Slim\Slim::getInstance()->urlFor('gerer_pochette', array('id' => $pochette->id)); ?>" method="POST">
                <div class="form-group">
                    <label for="email">Adresse email</label>
                    <input type="email" name="email" id="email" class="form-control">
                </div>
                <div class="form-group">
                    <label for="password">Mot de passe</label>
                    <input type="password" name="password" id="password" class="form-control">
                </div>
                <input type="submit" value="Gérer la pochette" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>
