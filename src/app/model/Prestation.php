<?php
/**
 * Created by PhpStorm.
 * User: alexis
 * Date: 11/02/16
 * Time: 09:53
 */

namespace app\model;


use Illuminate\Database\Eloquent\Model;

class Prestation extends Model{

    protected $table = 'prestation';

    protected $primaryKey = 'id';

    public $timestamps = false;

    public function getType() {
        return $this->belongsTo('app\model\Type', 'type');
    }

    public function notes() {
        return $this->hasMany('app\model\Notation');
    }

    public function moyenne() {
        $moyenne = 0;
        $nbNotes = 0;
        foreach($this->notes as $note) {
            $moyenne += $note->note;
            $nbNotes++;
        }

        return ($nbNotes === 0) ? 0 : $moyenne / $nbNotes;
    }

}