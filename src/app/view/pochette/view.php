<div class="container">
    <div class="page-header">
        <h1>Pochette</h1>
    </div>
    <?php echo $flash['message']; ?>
    <h3>Pochette</h3>
    <ul>
        <?php
            echo '
                <li>Nom : ' . $pochette->nom . '</li>
                <li>Message : ' . $pochette->message . '</li>
            ';
        ?>
    </ul>
    <h3>Prestations</h3>
    <ul>
        <?php
            foreach($pochette->prestations as $prestation) {
                echo '
                    <li>
                        <a href="' . \Slim\Slim::getInstance()->urlFor('view_prestation', array('id' => $prestation->id)) . '">' . $prestation->nom . '</a>
                        <a href="' . \Slim\Slim::getInstance()->urlFor('remove_prestation', array('id' => $prestation->id)) . '"><span class="glyphicon glyphicon-remove"></span></a>
                    </li>
                ';
            }
        ?>
    </ul>
    <a href="<?php echo \Slim\Slim::getInstance()->urlFor('valider'); ?>" class="btn btn-primary">Valider la pochette</a>
    <a href="<?php echo \Slim\Slim::getInstance()->urlFor('delete_pochette'); ?>" class="btn btn-danger">Supprimer la pochette</a>
</div>
